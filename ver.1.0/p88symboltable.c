#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "p88symboltable.h"

#define HASH_TABLE_BITW     10
#define HASH_TABLE_SIZE     (1 << HASH_TABLE_BITW)
#define HASH_TABLE_MASK     (HASH_TABLE_SIZE - 1)
#define HASH_INC            311 /* prime number */

//GROUP,JE,JNE追加
/* Strings should be arranged in the same order as operations. */
const char *oprations[] = {
    "END", "COPY",
    "ADD", "SUB", "MUL", "DIV", "CMP",
    "JMP", "JNB", "JB", "JNE", "JE",
    "CALL", "RTN",
    NULL
};

const char *oprations_ex[] = {//新たに分離したオペコード
  "PUSH","POP",
  "IN","OUT",
  NULL
};

const char *registers[] = {
    "AX", "BX", "CX", "DX",
    NULL
};

int labelcount;//グローバルラベル領域を保持する変数

static struct {//構造体
    char name[MAX_ID_LENGTH+1];
    long address;
    long address_ex;//PUSH,POP,IN,OUTのための判別アドレス
    int labelnum;//グローバルラベルの新出ごとに加算されるグローバルラベルの数(領域)
    unsigned char kind;
    /* Contents of address:
        k_opcode:   opcode,
        k_register: register number,
        k_label:    address,
        k_undef:    line number of src
     */
} hashtable[HASH_TABLE_SIZE];

#ifndef HAS_STRCASECMP
static int strcasecmp(const char *p, const char *q)
{
    int d;
    do {
        if (*p == 0 && *q == 0)
            return 0;
        d = tolower(*p) - tolower(*q);
        p++;
        q++;
    } while(d == 0);
    return d;
}
#endif

static int search_id(const char *str, int lnum)
{
    const int ChrUsed = 4;
    int i, v, x;

    /* mask 0x1f to ignore case of letters */
    v = (str[0] & 0x1f) << (HASH_TABLE_BITW - 5);
    for (i = 1; i < ChrUsed && str[i] != 0; i++)
        v ^= (str[i] & 0x1f) << (i - 1);
    x = v & HASH_TABLE_MASK;
    while (hashtable[x].kind != null){
        if(strcasecmp(hashtable[x].name, str) == 0){
            break;
        }
        x = (x + HASH_INC) & HASH_TABLE_MASK;
      }
    return x;
}

void init_table(void)
{
    int i, x;

    for (i = 0; i < HASH_TABLE_SIZE; i++){
        hashtable[i].kind = null;
        hashtable[i].labelnum = null;
      }
    for (i = 0; oprations[i]; i++) {
        x = search_id(oprations[i],0);
        strcpy(hashtable[x].name, oprations[i]);
        hashtable[x].kind = k_opcode;
        hashtable[x].address = i;
    }
    for (i = 0; oprations_ex[i]; i++) {//PUSH,POP,IN,OUTのための拡張したハッシュテーブル
      x = search_id(oprations_ex[i], 0);
      strcpy(hashtable[x].name, oprations_ex[i]);
      hashtable[x].kind = k_opcode;
      hashtable[x].address = op_GROUP;//4bitのオペコード枠で使用するオペコード識別値(op_GROUP)
      hashtable[x].address_ex = i;//メモリ5~6bit目で判別するための値
    }
    for (i = 0; registers[i]; i++) {
        x = search_id(registers[i], 0);
        strcpy(hashtable[x].name, registers[i]);
        hashtable[x].kind = k_register;
        hashtable[x].address = i;
    }


    x = search_id("RES", 0);   /* Directive to reserve memory area */
    strcpy(hashtable[x].name, "RES");
    hashtable[x].kind = k_directive;
    hashtable[x].address = 0;

    x = search_id("RANDOM", 0);   /* Special address */
    strcpy(hashtable[x].name, "RANDOM");
    hashtable[x].kind = k_label;
    hashtable[x].address = RANDOM_ADDR;
}

static void copylabel(int x, const char *s)/*受け取ったアセンブラコードを大文字にする*/
{
    int i;
    char *p = hashtable[x].name;
    for (i = 0; i < MAX_ID_LENGTH && s[i]; i++) {
        p[i] = toupper(s[i]);
    }
    p[i] = 0;
}

int def_label(const char *lab, long addr)
{
    int x, knd;

    x = search_id(lab, 0);
    knd = hashtable[x].kind;
    if (knd == null){ /* new label */
        copylabel(x, lab);//文字を大文字にする
      }
    if (knd == null || knd == k_undef) {
        hashtable[x].kind = k_label;
        hashtable[x].address = addr;
    }
    return knd;
}

int def_label_local(const char *lab, long addr, int *labelcount)//ローカルラベルが来た時のラベル登録
{
    int x, knd, lnum;

    x = search_id(lab, *labelcount);
    knd = hashtable[x].kind;
    lnum = hashtable[x].labelnum;
    if (lnum != *labelcount || knd == null){ /* new label */
        copylabel(x, lab);
      }
    if (knd == null || knd == k_undef || lnum != *labelcount) {//グローバルラベル域の異なる場合を追加
        hashtable[x].kind = k_label;
        hashtable[x].address = addr;
        hashtable[x].labelnum = *labelcount;//何個目のグローバルラベルの領域にあるかを指す
    }
    return knd;
}

int check_id(const char *str, int linenum, long *addrp, long *addrp_ex, int *labelcount)
{
    int x, knd, lnum;

    x = search_id(str, *labelcount);
    knd = hashtable[x].kind;
    lnum = hashtable[x].labelnum;
    if (knd == null || (str[0] == '@' && lnum != *labelcount)) {//`@`で、かつグローバルラベル域が異なるとき
        copylabel(x, str);
        hashtable[x].kind = k_undef;
        hashtable[x].address = linenum;
        hashtable[x].address_ex = linenum;//PUSH,POP,IN,OUTのための判別値
        hashtable[x].labelnum = *labelcount;//現在のグローバルラベル領域を格納
    }else if ((knd != k_undef && addrp != NULL))
        *addrp = hashtable[x].address;
        if(knd == k_opcode && *addrp == op_GROUP){
          *addrp_ex = hashtable[x].address_ex;
        }
    return knd;
}

long undef_words(long memsize)
{
    int i;
    long count = memsize;

    for (i = 0; i < HASH_TABLE_SIZE; i++) {
        if (hashtable[i].kind == k_undef) {
            fprintf(stderr, "!!Undefined Label ... Memory allocated: ");
            fprintf(stderr, "%s = [%ld], Line=%ld\n",
                hashtable[i].name, count,
                hashtable[i].address + 1 /* line of src */);
            hashtable[i].kind = k_label;
            hashtable[i].address = count;
            count += 2;
        }
    }
    return count;
}
