#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "p88common.h"
#include "p88symboltable.h"
#include "p88token.h"

/* extern */
long curline;    /* currentline */


int get_token(const char *p, int *lengthp)//ポインタにして唯一の返り値以外に*lengthpに値を入れている。
{

    int x = 0;

    if (p[x] == 0 || p[x] == ';') {//一文字目(ラベル)がないとエラー
        *lengthp = 0;

        return t_eol; /* End Of Line */
    }
    if (isalpha(p[x]) || p[x] == '@') {
        ++x;

        while (isalnum(p[x]))
            ++x;
        *lengthp = x;

        return t_name;
    }
    if (isdigit(p[x]) || p[x] == '-' || p[x] == '+') {//levelで使う
        ++x;

        while (isdigit(p[x]))
            ++x;
        *lengthp = x;

        return t_number;
    }
    *lengthp = 1;
    return t_symbol;
}

void namecpy(char *buf, const char *p, int n)//メモリサイズに合わして７文字だけbufにとる
{
    int i, len;
    len = (n < MAX_ID_LENGTH) ? n : MAX_ID_LENGTH;
    for (i = 0; i < len; i++)
        buf[i] = p[i];
    buf[i] = 0;
}

long get_literal(const char *lp, int bits)//
{
    /* returns integer in specified bits including sign bit */
    /* returns -1 if error */
    long literal;
    unsigned long w, mask;

    literal = atol(lp);//文字列をlong型に変換しれ入れる
    mask = ~0UL << (bits - 1);//1がたったビットを左にずらして0を入れる
    w = literal & mask;
    if (w != 0 && w != mask)
        return -1;
    return ((unsigned long)literal & ((1UL << bits) - 1));
}

static int get_indirect(const char *p, int *lengthp, int *regp)
{//間接アドレッシング
    int length;
    long addr,addr_ex;
    const char *origp = p;
    char buf[MAX_ID_LENGTH+1];

    if ((*p != 'c' && *p != 'C') || *(p+1) != '(') {
        *lengthp = 0;
        return 0; /* not indirect */
    }
    p += 2;
    skip_space(p);
    if (get_token(p, &length) != t_name)
        return -1; /* error */
    namecpy(buf, p, length);
    if (check_id(buf, curline, &addr, &addr_ex, &labelcount) != k_register)
        return -1; /* error */
    p += length;
    skip_space(p);
    if (*p++ != ')')
        return -1; /* error */
    *lengthp = p - origp;
    *regp = addr;
    return 1; /* success */
}

operand get_operand(const char *p, int *lengthp)
{
    operand info = { 0 };
    int length, token, ch, knd;
    int x, reg;
    long literal, addr, addr_ex;
    const char *origp = p;
    char buf[MAX_ID_LENGTH+1];

    info.typ = t_none; /* initial value */
    skip_space(p);
    if ((x = get_indirect(p, &length, &reg)) != 0) {
        if (x == 1) { /* indirect found */
            info.typ = t_reg_indirect;
            info.reg = reg;
        }else if (x < 0) /* error */
            info.typ = t_error;
        *lengthp = (p - origp) + length;
        return info;
    }

    token = get_token(p, &length);
    if (token == t_eol){
        info.typ = t_error;
    }else if (token == t_symbol) {
        ch = *p;
        p += length;
        if (ch == '#') {
            literal = get_literal(p, 8);
            if (literal < 0)
                info.typ = t_error;
            else {
                info.typ = t_value;
                info.val = literal;
            }
            if (*p == '-' || *p == '+')
                p++;
            while (isdigit(*p))
                p++;
        }else
            info.typ = t_error;
    }else { /* t_name or t_number */
        int isreg = NO;
        info.typ = t_mem_only;
        if (token == t_name) {
            namecpy(buf, p, length);
            knd = check_id(buf, curline, &addr, &addr_ex, &labelcount);
            if (knd == k_register) {
                isreg = YES;
                info.typ = t_reg_only;
                info.reg = addr;
            }else if (knd == k_opcode){
                info.typ = t_error;
            }else{
                info.val = addr;
            /* null, k_label, or k_undef are allowed */
          }
        }else { /* t_number */
            literal = get_literal(p, 16);
            if (literal < 0)
                info.typ = t_error;
            else
                info.val = literal;
        }
        p += length;
        skip_space(p);
        if (isreg == NO && *p == '+') {
            ++p;
            skip_space(p);
            x = get_indirect(p, &length, &reg);
            if (x == 1) { /* indirect found */
                info.typ = t_mem_register;
                info.reg = reg;
            }else /* error */
                info.typ = t_error;
            p += length;
        }
    }

    *lengthp = p - origp;
    return info;
}

int gen_template(unsigned char *memp,
    int typ, int op, int r1, int r2, int store, long addr)
{
    int fbits1, fbits2, withaddr;

    switch (typ) {
    case t_reg_only:
        fbits1 = 1, fbits2 = 0, withaddr = NO;
        break;
    case t_mem_only:
        fbits1 = 2, fbits2 = 0, withaddr = YES;
        r2 = 0;
        break;
    case t_reg_indirect:
        fbits1 = 1, fbits2 = 2, withaddr = NO;
        break;
    case t_mem_register:
        fbits1 = 3, fbits2 = 2, withaddr = YES;
        break;
    default:
        return -1; /* error */
    }
    if (store)
        fbits2 |= 01;   /* STORE-type COPY operation: COPY mem,ax */
    memp[0] = (op << 4) | (fbits1 << 2) | r1;
    memp[1] = (fbits2 << 6) | (r2 << 4);
    if (withaddr) {
        memp[1] |= (addr >> 8) & 0x0f;
        memp[2] = addr;
        return 3;
    }
    return 2;
}
