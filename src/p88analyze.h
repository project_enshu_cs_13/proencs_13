#ifndef _P88_ANALYZE_H_
#define _P88_ANALYZE_H_
#include "p88common.h"

extern long bytecount;  /* bytes of machine code */

int analyze(const char *const *prog, long lines, int pass, unsigned char *core);

#endif
