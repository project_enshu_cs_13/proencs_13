#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <setjmp.h>
#include <signal.h>
#include "p88common.h"
#include "p88simulator.h"
#include "p88env.h"

static unsigned long max_addr;  /* MAX address kept by source program */
static jmp_buf jmpenv;  /* long jump; in order to return from signal */

#define INBUF_SZ    64

static int exec_a_step(int trace)
{
  int ptflag= NO;
  int opcode;
  int opcode_ex;//
  int length = 1;
  int status = st_running; /* default */
  int rx, jmpflag, top = 0;
  operand info;
  long data, addr;

  if (pc >= max_addr)
  return st_outOfMem;
  opcode = mem[pc] >> 4;
  switch (opcode) {
    case op_HALT:
    length = 0;
    if (trace)
    print_trace(opcode, NULL,ptflag);
    status = st_halt;
    break;
    case op_COPY:
    info = inspect_code(&mem[pc], &length);//pcプログラムカウンタ(現在の位置
      if (trace)
      print_trace(opcode, &info,ptflag);
      if (! IS_STORE(info.flags)) { /* Load-type */
        data = fetch_data(info, &status);
        if (status == st_running)
        regs[info.reg2] = data;

        break;
      }
      /* Store-type */
      addr = 0;
      switch (info.typ) {
        case t_mem_only: /* address */
        addr = info.val;
        break;
        case t_reg_indirect: /* register indirect */
        addr = regs[info.reg];
        break;
        case t_mem_register: /* address + register indirect */
        addr = info.val + regs[info.reg];
        break;
        default: /* t_reg_only || t_value */
        status = st_illegalOperand;
        break;
      }
      if (addr > MAX_ADDRESS){
        status = st_illegalMemory;
      }
      if (status == st_running) {
        mem[addr] = regs[info.reg2] >> 8;
        mem[addr + 1] = regs[info.reg2];
      }
      break;
      case op_ADD:
      case op_SUB:
      case op_MUL:
      case op_DIV:
      case op_CMP:
      info = inspect_code(&mem[pc], &length);
      if (trace)
      print_trace(opcode, &info, ptflag);
      if (info.typ == t_reg_indirect
        || info.typ == t_mem_register) {
          status = st_illegalOperand;
          break;
        }
        data = fetch_data(info, &status);
        if (status != st_running)
        break;
        if (opcode == op_CMP) {
          cf = (regs[info.reg2] < data) ? CF_BELOW : CF_NotBELOW;
          ef = (regs[info.reg2] == data) ? EF_EQUAL : EF_NotEQUAL;//追加
          break;
        }
        if (opcode == op_DIV && data == 0) {
          status = st_zeroDiv;
          break;
        }
        switch (opcode) {
          case op_ADD: regs[info.reg2] += data;  break;
          case op_SUB: regs[info.reg2] -= data;  break;
          case op_MUL: regs[info.reg2] *= data;  break;
          case op_DIV: regs[info.reg2] /= data;  break;
        }
        break;
        case op_JMP:
        case op_JNB:
        case op_JB:
        case op_JNE:
        case op_JE:
        length = 2;
        jmpflag = YES;
        switch (opcode) {
            case op_JNB: jmpflag = (cf == CF_NotBELOW); break;
            case op_JB:  jmpflag = (cf == CF_BELOW);    break;
            case op_JNE:  jmpflag = (ef == EF_NotEQUAL); break;
            case op_JE: jmpflag = (ef == EF_EQUAL); break;
            case op_JMP:
            default: break;
        }
        addr = ((mem[pc] << 8) | mem[pc+1]) & 0x0fff;
        if (trace) {
          info.typ = t_mem_only;
          info.val = addr;
          print_trace(opcode, &info,ptflag);
        }
        if (jmpflag) {
          //printf("pc_before: %lu\n", pc);
          pc = addr;
          //printf("pc_after: %lu\n", pc);
          return status; /* JUMP!! */
        }
        break;
        case op_CALL:
        addr = ((mem[pc] << 8) | mem[pc+1]) & 0x0fff;
        if (trace) {
          info.typ = t_mem_only;
          info.val = addr;
          print_trace(opcode, &info, ptflag);
        }
        top = (regs[SP] & ~01) - 2;
        if (top <= max_addr) {
          status = st_illegalMemory;
          break;
        }
        pc += 2;    /* length == 2 */
        mem[top] = pc >> 8;
        mem[top+1] = pc & 0xff;
        regs[SP] = top;
        pc = addr;
        return status; /* JUMP!! */
        case op_RTN:
        if (trace){
          print_trace(opcode, NULL,ptflag);
        }
        top = regs[SP] & ~01;
        if (top >= STACK_BASE) {
          status = st_illegalMemory;
          break;
        }
        pc = (mem[top] << 8) | mem[top+1];
        regs[SP] = top + 2;
        return status; /* JUMP!! */
        case op_GROUP://
        ptflag=YES;
        opcode_ex = mem[pc] >> 2;
        opcode_ex = opcode_ex & 3;
        switch(opcode_ex){
          case op_PUSH:
          case op_POP:
          rx = mem[pc] & 0x03;
          if (trace) {
            info.reg2 = rx;
            print_trace(opcode, &info, ptflag);
          }
          top = regs[SP] & ~01;
          if (opcode_ex == op_PUSH) {
            top -= 2;
            if (top <= max_addr) {
              status = st_illegalMemory;
              break;
            }
            mem[top] = regs[rx] >> 8;
            mem[top+1] = regs[rx] & 0xff;
            regs[SP] = top;
          }else { /* POP */
            if (top >= STACK_BASE) {
              status = st_illegalMemory;
              break;
            }
            regs[rx] = (mem[top] << 8) | mem[top+1];
            regs[SP] = top + 2;
          }
          break;
          case op_IN:
          rx = mem[pc] & 0x03;
          if (trace) {
            info.reg2 = rx;
            print_trace(opcode, &info, ptflag);
          }
          fprintf(stderr, "in ? ");
          for ( ; ; ) {
            char inbuf[INBUF_SZ];
            int x = 0;
            if (fgets(inbuf, INBUF_SZ, stdin) == NULL) {
              status = st_EOF;
              break;
            }
            while (inbuf[x] == ' ' || inbuf[x] == '\t'){
              x++;
            }
            if (isdigit(inbuf[x]) || inbuf[x] == '-') {
              regs[rx] = atoi(&inbuf[x]);
              break;
            }
            fprintf(stderr, "input number ? ");
          }
          break;
          case op_OUT:
          rx = mem[pc] & 0x03;
          if (trace) {
            info.reg2 = rx;
            print_trace(opcode, &info, ptflag);
          }
          printf("out: %d\n", regs[rx]);
          break;
        }
        break;
        default:
        if (trace){
          print_trace(opcode, NULL, ptflag);
        }
        status = st_illegalOpcode;
        break;
      }
      if (status == st_running) {
        pc += length;
      }
      return status;
    }

    static void interrupt(int sig)
    {
      signal(SIGINT, SIG_DFL);
      longjmp(jmpenv, 1);
    }

    /* extern */
    void simulate(unsigned char *core, long maxaddress, int trace)
    {
      int i, st;

      mem = core;
      max_addr = maxaddress;
      cf = CF_CLEAR;
      ef = EF_CLEAR;
      pc = 0x00;
      for (i = 0; i < REGISTERS; i++)
      regs[i] = 0;
      regs[SP] = STACK_BASE;
      signal(SIGINT, interrupt);
      if (setjmp(jmpenv) == 0) {
        do {
          st = exec_a_step(trace);
        }while (st == st_running);
        printf(">>> Program terminated. (%s)\n", reason_str[st]);
        signal(SIGINT, SIG_DFL);
      }else {
        printf("!!>>> Program is interrupted. IP=%ld\n", pc);
      }
    }
