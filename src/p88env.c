#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "p88common.h"
#include "p88simulator.h"
#include "p88symboltable.h"
#include "p88env.h"
#if !defined(USE_SRANDOMDEV)
#include <time.h>
#endif

/* extern */
//env.hにて宣言されてるグローバル変数を定義している
unsigned char *mem;
unsigned long pc; //条件分岐？
unsigned int cf;
unsigned int ef;//ef条件を入れるためのフラグ変数
short regs[REGISTERS];
//REGISTER=4 common.h

/* extern */
const char *reason_str[] = {//アセンブラ実行時のエラーメッセージを格納
    "running",
    "Halt",
    "out of memory",
    "illegal code",
    "illegal operand",
    "illegal memory access",
    "div by zero",
    "unexpected EOF"
};

static void ins_regs(char *p, int pos)
{
  //空白を詰めて見栄えをよくする
  //pとposに入っているのはbufとposition
  //posは１８が入ってる
  //bufは８０の配列で
  int len = strlen(p); //pの長さを入れている
    if (len < pos)
        while (len < pos)
	  p[len++] = ' '; //lenに＋１しつつ空白入れてく
    else
        p[len++] = ' ';
}


/*トレースモードで作動するメモリのトレースのための関数*/
void print_trace(int opcode, operand *infop, int ptflag)
{
    const int position = 18;
    char buf[80], *bp;
    short n;

    int cpflag = (opcode == op_COPY && IS_STORE(infop->flags));
    if(ptflag){
    printf(". . . . . . . %4ld:  %-4s  ", pc, oprations_ex[opcode]);
    }else{
      printf(". . . . . . . %4ld:  %-4s  ", pc, oprations[opcode]);
	}
    if (infop == NULL) {
        buf[0] = 0;
        ins_regs(buf, position);
        printf("%s\n", buf);
        return;
    }
    //.hのところの#define IS_MEMONLY(x)   ((x) >= op_PUSH && (x) <= op_OUT)
    if (IS_MEMONLY(opcode)) {
        sprintf(buf, "%s", registers[infop->reg2]);
        ins_regs(buf, position);
        printf("%s\n", buf);
        return;
    }
    //IS_BRANCH(x)    ((x) >= op_JMP && (x) <= op_CALL)
    if (!cpflag && !IS_BRANCH(opcode)) {
        sprintf(buf, "%s,", registers[infop->reg2]);
        bp = buf + strlen(buf);
    }else
        bp = buf;
    switch (infop->typ) {
    case t_reg_only: /* register direct */
        sprintf(bp, "%s", registers[infop->reg]);
        break;
    case t_mem_only: /* address */
        sprintf(bp, "[%ld]", infop->val);
        break;
    case t_reg_indirect: /* register indirect */
        sprintf(bp, "c(%s)", registers[infop->reg]);
        break;
    case t_mem_register: /* address + register indirect */
        sprintf(bp, "[%ld]+c(%s)", infop->val, registers[infop->reg]);
        break;
    case t_value: /* literal value */
        n = infop->val;    /* convert into signed short */
        sprintf(bp, "#%d", n);
        break;
    default:
        break;
    }
    if (cpflag) {
        bp += strlen(bp);
        sprintf(bp, ",%s", registers[infop->reg2]);
    }
    ins_regs(buf, position);
    //printf("%s\n", buf);
}


//ここではp[0]とp[1]に入るものをビット演算することで即値かどうかの判定を行なっている。
operand inspect_code(unsigned char *p, int *opleng)
{
    unsigned int ibits;
    //ビット列として扱いたいためシフト演算なんかで不必要なシフト演算が起きることを防ぐため
    static operand info;
    //機械語を扱いやすくした形にして入れてる
    //0xは１６進数
    info.reg2 = p[0] & 0x03; //memp[0]の7~8bit目の取得(レジスタ１)
    info.reg = (p[1] >> 4) & 0x03;//memp[1]のレジスタ２の値をとる
    info.flags = (p[0] & 0x0c) | (p[1] >> 6);
    ibits = (p[0] >> 2) & 0x03;

    switch (ibits) {
    case 0: /* immediate */
        info.typ = t_value;
	//pはもともとunsigned charなのを読み替えるのは符号拡張するため
	//符号付きの整数
        info.val = ((signed char *)p)[1];
        *opleng = 2;
        break;
    case 1:
        info.typ = (p[1] & 0x80) ? t_reg_indirect : t_reg_only;
        *opleng = 2;
        break;
    case 2:
    case 3:
        info.typ = (ibits == 2) ? t_mem_only : t_mem_register;
        info.val = ((p[1] << 8) | p[2]) & 0x0fff;
        *opleng = 3;
        break;
    }
    return info;
}

static short shramdom(void)
{
    static int initialized = 0;

#if defined USE_SRANDOMDEV
    if (initialized == 0) {
        srandomdev();
        initialized = 1;
    }
    return (random() & 0x7fff);
#elif defined USE_RANDOM
    if (initialized == 0) {
        unsigned long seed = time(NULL);
        srandom(seed);
        initialized = 1;
    }
    return (random() & 0x7fff);
#else
    if (initialized == 0) {
        unsigned int seed = (unsigned int)time(NULL);
        srand(seed);
        initialized = 1;
    }
    return (rand() & 0x7fff);
#endif
}


//print_traceによって判定されたレジスタやアドレスの情報から値を返したりaddrの中に入れる情報を場合分けしている。
long fetch_data(operand info, int *errcode)
{
    long addr;
    short n;

    *errcode = st_running;
    switch (info.typ) {
    case t_reg_only: /* register direct */
        return regs[info.reg];
    case t_mem_only: /* address */
        addr = info.val;
        break;
    case t_reg_indirect: /* register indirect */
        addr = regs[info.reg];
        break;
    case t_mem_register: /* address + register indirect */
        addr = info.val + regs[info.reg];
        break;
    case t_value: /* literal value */
    default:
        n = info.val;  /* convert into signed short value */
        return n;
    }

    //
    if (addr > MAX_ADDRESS) {
        if (addr == RANDOM_ADDR){
            return shramdom();
          }
        *errcode = st_illegalMemory;
        return 0; /* Error */
    }
    n = ((mem[addr] << 8) | mem[addr + 1]);    /* convert into signed short */
    return n;
}
