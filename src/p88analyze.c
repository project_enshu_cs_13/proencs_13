#include <stdio.h>
#include "p88common.h"
#include "p88symboltable.h"
#include "p88token.h"

static char *Err_Unexpect = "Unexpected token";
static char *Err_1stOperand = "1st operand is illegal";
static char *Err_2ndOperand = "2nd operand is illegal";

/* extern */
long bytecount;         /* bytes of machine code */

static const char *gen_code(int opcode, int opcode_ex, const char *lp,
    unsigned char *memoryp, int *gen)
{
    int length, blen;
    operand oprnd = { 0 }, oprnd2 = { 0 };//初期化
    unsigned char *memp;
    unsigned char tmppool[8]; /* used if pas s == 1 */

    *gen = 0; /* for error */
    memp = (memoryp)
        ? memoryp    /* pass2 */
        : tmppool;    /* pass1 */
    if (opcode == op_HALT || opcode == op_RTN) {
        memp[0] = opcode << 4;
        *gen = 1;
        return NULL;
    }
    oprnd = get_operand(lp, &length);
    if (oprnd.typ == t_error)
        return "Syntax error in 1st operand";
    lp += length;
    skip_space(lp);

    switch (opcode) {
    case op_ADD:
    case op_SUB:
    case op_MUL:
    case op_DIV:
    case op_CMP: /* 2 operands */
        if (oprnd.typ != t_reg_only)
            return "1st operand isn't register";
        /* fall to next case */
    case op_COPY:
        if (*lp++ != ',')
            return "',' is expected";
        skip_space(lp);
        oprnd2 = get_operand(lp, &length);
        if (oprnd2.typ == t_error)
            return "Syntax error in 2nd operand";
        lp += length;
        break;
    case op_JMP:
    case op_JNB:
    case op_JB:
    case op_JNE:
    case op_JE:
    case op_CALL: /* only address is allowed */
        if (oprnd.typ != t_mem_only)
            return "Operand isn't address";
        break;
    case op_GROUP://op_GROUPの時にPUSH,POP,IN,OUTを判別する
    switch (opcode_ex) {
      case op_PUSH:
      case op_POP:
      case op_IN:
      case op_OUT:
      default: /* IO: only register is allowed */
          if (oprnd.typ != t_reg_only)
              return "Operand isn't register";
          break;
    }
    break;
    default: /* IO: only register is allowed */
        if (oprnd.typ != t_reg_only)
            return "Operand isn't register";
        break;
    }
    skip_space(lp);
    if (get_token(lp, &length) != t_eol)
        return Err_Unexpect;

    switch (opcode) {
    case op_COPY:
        if (oprnd.typ == t_reg_only) {
            /* LOAD operation */
            if (oprnd2.typ == t_value) {
                memp[0] = (opcode << 4) | oprnd.reg;
                memp[1] = oprnd2.val;
                *gen = 2;
                return NULL; /* success */
            }
            blen = gen_template(memp, oprnd2.typ, opcode,
                oprnd.reg, oprnd2.reg, 0, oprnd2.val);
            if (blen < 0)
                return Err_2ndOperand;
            *gen = blen;
            return NULL; /* success */
        }
        if (oprnd2.typ != t_reg_only)
            return "2nd operand isn't register";

        /* COPY(STORE) operation */
        switch (oprnd.typ) {
        case t_mem_only:
        case t_reg_indirect:
        case t_mem_register:
            blen = gen_template(memp, oprnd.typ, op_COPY,
                oprnd2.reg, oprnd.reg, 1, oprnd.val);
            if (blen < 0)
                return Err_1stOperand;
            *gen = blen;
            break;
        default:
            return Err_Unexpect;
        }
        break; /* success */
    case op_ADD:
    case op_SUB:
    case op_MUL:
    case op_DIV:
    case op_CMP:
        switch (oprnd2.typ) {
        case t_reg_only:
        case t_mem_only:
            blen = gen_template(memp, oprnd2.typ, opcode,
                oprnd.reg, oprnd2.reg, 0, oprnd2.val);
            if (blen < 0)
                return Err_2ndOperand;
            *gen = blen;
            break;
        case t_value:
            memp[0] = (opcode << 4) | oprnd.reg;
            memp[1] = oprnd2.val;
            *gen = 2;
            break;
        default:
            return Err_Unexpect;
        }
        break; /* success */
    case op_JMP:
    case op_JNB:
    case op_JB:
    case op_JNE:
    case op_JE:
    case op_CALL:
        memp[0] = (opcode << 4) | ((oprnd.val >> 8) & 0x0f);
        memp[1] = oprnd.val;
        *gen = 2;
        break; /* success */
            //bit調整
    case op_GROUP://op_GROUPの時にPUSH,POP,IN,OUTを判別する
        switch (opcode_ex) {//5~6bit目に判別用の2ビットの値を入れる
          case op_PUSH:
          case op_POP:
          case op_IN:
          case op_OUT:
            memp[0] = (opcode << 4) | (opcode_ex<<2) | (oprnd.reg & 0x03);//判別用コードを2bit左シフトで挿入
            *gen = 1;
            break; /* success */
        }
        break;
    default:
        return "System Error: Code generation";
    }
    return NULL;
}

int analyze(const char *const *prog, long lines, int pass, unsigned char *core)
{
    int errcount = 0;
    int lastopcode = -1;
    const char *errmsg;
    labelcount = 0;

    bytecount = 0;
    for (curline = 0; curline < lines; curline++) {
        int length, token, knd, gen;
        long info, info_ex, literal;
        char buf[MAX_ID_LENGTH+1];
        const char *lp;
        unsigned char *memp;

        lp = prog[curline];
        if (*lp == ';' || *lp == 0) /* end of line */
            continue;

        /* label */
        if (*lp != ' ' && *lp != '\t') {
            token = get_token(lp, &length);
            if (token != t_name) {
                errmsg = Err_Unexpect;
                goto ErrContinue;
            }
            if (pass == 1) {/*ローカルラベルかグローバルラベルかを判別*/
                namecpy(buf, lp, length);
                if(*lp == '@'){
                  knd = def_label_local(buf, bytecount, &labelcount);
                  //現在のグローバルラベルの領域の値を元に判別とアドレス登録を行う
                }else{
                  labelcount++;//新しいグローバルラベル領域として更新
                  knd = def_label(buf, bytecount);
                }
                if (knd != null && knd != k_undef) {
                    errmsg = (knd == k_label)
                    ? "Duplicated label definitionl"
                    : Err_Unexpect;
                    goto ErrContinue;
                }
            }else{
                if(*lp !='@'){
                  labelcount++;
                }
            }
            lp += length;
        }
        skip_space(lp);

        /* op-code or data literal */
        token = get_token(lp, &length);
        switch (token) {
        case t_eol:
            continue; /* continue loop */
        case t_symbol:
            errmsg = Err_Unexpect;
            goto ErrContinue;
        case t_name:
            namecpy(buf, lp, length);
            knd = check_id(buf, curline, &info, &info_ex, &labelcount);
            lp += length;
            skip_space(lp);
            switch (knd) {
            case k_opcode:
                memp = (pass == 1) ? NULL : &core[bytecount];
                errmsg = gen_code(info, info_ex, lp, memp, &gen);
                lastopcode = info;
                if (errmsg)
                    goto ErrContinue;
                bytecount += gen;
                break;
            case k_directive: /* Directive RES */
                token = get_token(lp, &length);
                if (token != t_number
                || (literal = get_literal(lp, 13)) < 0) {
                    /* Unsigned 12bit number */
                    errmsg = "Number is expected";
                    goto ErrContinue;
                }
                if (pass == 2) {
                    long i;
                    for (i = 0; i < literal; i++)
                        core[bytecount++] = 0;
                }else
                    bytecount += literal;
                lp += length;
                goto CheckEOL;
            case null:
            case k_undef:
            case k_label:
                if (pass == 2) {
                    core[bytecount] = info >> 8;
                    core[bytecount+1] = info;
                }
                bytecount += 2;
                goto CheckEOL;
            default:
                errmsg = "Opcode is expected";
                goto ErrContinue;
            }
            break;
        case t_number:
            literal = get_literal(lp, 16);
            if (literal < 0) {
                errmsg = "Value exceeds the range";
                goto ErrContinue;
            }
            if (pass == 2) {
                core[bytecount] = literal >> 8;
                core[bytecount+1] = literal;
            }
            bytecount += 2;
            lp += length;
            goto CheckEOL;
        }

        continue;
CheckEOL:
        skip_space(lp);
        token = get_token(lp, &length);
        if (token == t_eol)
            continue; /* OK */
        errmsg = "End of line is expected";
ErrContinue:
        errcount++;
        fprintf(stderr, "*pass=%d, %s\n", pass, errmsg);
        fprintf(stderr, "%7ld>%s\n", curline+1, prog[curline]);
    }
    if (errcount == 0 && lastopcode != op_HALT) {
        if (pass == 2)
            core[bytecount] = op_HALT << 4;
        bytecount++;
    }
    return errcount;
}
