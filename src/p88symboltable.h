#ifndef _P88_SYMBOL_TABLE_H_
#define _P88_SYMBOL_TABLE_H_
#include "p88common.h"

extern const char *oprations[];
extern const char *oprations_ex[];
extern const char *registers[];

extern int labelcount;

void init_table(void);
int def_label(const char *lab, long addr);
int def_label_local(const char *lab, long addr, int *labelcount);//@がきたときに作動する関数
int check_id(const char *str, int linenum, long *addrp,long *addrp_ex, int *labelcount);
long undef_words(long);

#endif
