#if !defined(_P88_TOKEN_H_)
#define _P88_TOKEN_H_

#include "p88common.h"

#define skip_space(p)  while (*(p)==' ' || *(p)=='\t') (p)++

extern long curline;    /* current line */

int get_token(const char *p, int *lengthp);
void namecpy(char *buf, const char *p, int n);
long get_literal(const char *lp, int bits);
operand get_operand(const char *p, int *lengthp);
int gen_template(unsigned char *memp,
    int typ, int op, int r1, int r2, int store, long addr);

#endif /* _P88_TOKEN_H_ */
