#if !defined(_P88_ENV_H_)
#define _P88_ENV_H_

#define CF_CLEAR        0
#define EF_CLEAR        0
#define CF_NotBELOW     0
#define CF_BELOW        1
#define EF_NotEQUAL     0
#define EF_EQUAL        1

//env.cに出てくる時に入る条件↓
#define IS_STORE(x)     ((x) & 01)
#define IS_MEMONLY(x)   ((x) >= op_PUSH && (x) <= op_OUT)
    /* opcode == op_PUSH, op_POP, op_IN, op_OUT */
#define IS_BRANCH(x)    ((x) >= op_JMP && (x) <= op_CALL)
    /* opcode == op_JMP, op_JNB, op_JB, op_CALL */

//グローバル変数の定義が他の場所で行われていることをコンパイラに知らせる
extern unsigned char *mem;
extern unsigned long pc;
extern unsigned int cf;
extern unsigned int ef;
//extern unsigned int cf_e; //
extern short regs[REGISTERS];

enum {
    st_running = 0,
    st_halt, st_outOfMem, st_illegalOpcode, st_illegalOperand,
    st_illegalMemory, st_zeroDiv, st_EOF
};
extern const char *reason_str[];

void print_trace(int opcode, operand *infop, int ptflag);
operand inspect_code(unsigned char *p, int *opleng);
long fetch_data(operand info, int *errcode);

#endif /* _P88_ENV_H_ */
