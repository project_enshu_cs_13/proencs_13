#ifndef _P88_COMMON_H_
#define _P88_COMMON_H_

#include "p88_config.h"

#define VERSION     "1.04"
#define COPYRIGHT   "(C) 2006, 2007, 2017, Takeshi Ogihara"

#define MAX_ID_LENGTH   7
#ifndef YES
#define YES     1
#define NO      0
#endif

#define REGISTERS       4
#define SP              3       /* DX is the stack pointer */
#define RANDOM_ADDR     0xffe   /* special address */
#define STACK_BASE      0xffe   /* Can't store into this address */
#define MAX_ADDRESS     0xffd

/* kind of token */
enum {
    t_eol,  /* End Of Line */
    t_symbol, t_name, t_number//tesuto
};

/* kind of string */
enum {
    null = 0,
    k_opcode, k_register, k_directive, k_label, k_undef
};

/* operations */
//op_JE,op_JNE追加と元の4つを束ねるop_GROUPの追加
enum {
    op_HALT, op_COPY,
    op_ADD, op_SUB, op_MUL, op_DIV, op_CMP,
    op_JMP, op_JNB, op_JB, op_JNE, op_JE,
    op_CALL, op_RTN, op_GROUP
};

/*operations of op_GROUP*/
//PUSH,POP,IN,OUTのためのコード(op_GROUPに引っかかるとこれで判別する)
enum{
  op_PUSH, op_POP,
  op_IN, op_OUT
};

/* instruction & addressing mode */
enum {
    t_none,
    t_error,
    t_reg_only,     /* register direct */
    t_mem_only,     /* address */
    t_reg_indirect, /* register indirect */
    t_mem_register, /* address + register indirect */
    t_value         /* literal value */
};

typedef struct {
    unsigned char   typ;
    unsigned char   reg;
    unsigned char   reg2;
    unsigned char   flags;  /* op[ ----ar--, id------ ] => [----arid] */
    long            val;
} operand;

#endif /* _P88_COMMON_H_ */
